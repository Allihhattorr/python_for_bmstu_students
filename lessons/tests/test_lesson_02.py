import re

from lesson_02.print_students import students

# В переменной ниже используется регулярное выражение https://w.wiki/4dir Смысл
# регулярного выражения ниже:
# - три слова на кириллице
# - слова разделены пробелами
# - слова начинаются с заглавной буквы с последующими строчными буквами
# - фамилия (первое слово) может быть составной (но не обязательно), т.е. содержащей
#   тире посередине
# - слово после тире (если фамилия составная) также должно начинаться с заглавной буквы
# - вокруг тире не должно быть пробелов (если фамилия составная)
STUDENT_NAME_TEMPLATE = re.compile(
    r"[А-ЯЁ][а-яё]+(-[А-ЯЁ][а-яё]+)? [А-ЯЁ][а-яё]+ [А-ЯЁ][а-яё]+"
)


def name_is_properly_formatted(name: str) -> bool:
    return re.fullmatch(STUDENT_NAME_TEMPLATE, name) is not None


def test_students_sorted_alphabetically():
    for group_students in students.values():
        sorted_group_students = sorted(group_students)
        assert all([a == b for a, b in zip(group_students, sorted_group_students)])


def test_all_names_are_properly_formatted():
    for group_students in students.values():
        assert all(
            [
                name_is_properly_formatted(student_name)
                for student_name in group_students
            ]
        )
