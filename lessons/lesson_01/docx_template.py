"""
Пример автоматизации отчёта в word

Документацию можно почитать здесь:
https://docxtpl.readthedocs.io/en/latest/index.html
https://docxtpl.readthedocs.io/_/downloads/en/latest/pdf/

Или статья на русском:
https://docs-python.ru/packages/modul-python-docx-python/modul-docx-template/
"""

# %%
import datetime
import random

import matplotlib.pyplot as plt
from docx.shared import Cm
from docxtpl import DocxTemplate, InlineImage, Listing
from sympy import expand, symbols
from utils.sympy2subdoc import sympy_expr_to_subdoc

# %%
# Импортируем шаблон
docx_template = DocxTemplate("lessons/lesson_01/automated_report_template.docx")


# %%
# Создаём случайные данные для заполнения таблицы и построения графика
table_contents = []
x = []
y = []

for i in range(0, 12):
    number = round(random.random(), 3) + i
    table_contents.append({"index": i, "value": f"{number:.3f}"})
    x.append(i)
    y.append(number)

# Отображаем и сохраняем график
fig = plt.figure()
plt.plot(x, y)
fig.savefig("image.png", dpi=fig.dpi)

# Импортируем картинку с сохраненным графиком в документ
image = InlineImage(docx_template, "image.png", Cm(12))

# %%
# Прочитаем код из какого-нибудь скрипта для вставки в отчёт
with open("lessons/lesson_01/utils/sympy2subdoc.py", "r") as f:
    the_listing_with_newlines = f.read()


# %%
# Создадим переменные и формулу
x, y = symbols("x y")
expr = (x + y) ** 2
expr

# %%
expand(expr)

# %%
initial_formula = sympy_expr_to_subdoc(expr, docx_template=docx_template)
expanded_formula = sympy_expr_to_subdoc(expand(expr), docx_template=docx_template)

# %%
# Объявляем временные переменные
context = {
    "title": "Гиперболический параболоид",
    "day": datetime.datetime.now().strftime("%d"),
    "month": datetime.datetime.now().strftime("%b"),
    "year": datetime.datetime.now().strftime("%Y"),
    "table_contents": table_contents,
    "image": image,
    "listing": Listing(the_listing_with_newlines),
    "initial_formula": initial_formula,
    "expanded_formula": expanded_formula,
}

# Render automated report
docx_template.render(context)
docx_template.save("generated_report.docx")

# %%
